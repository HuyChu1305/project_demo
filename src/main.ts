import './assets/style.scss';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import axios from 'axios';
import VueAxios from 'vue-axios';

import { createApp } from 'vue';
import { createPinia } from 'pinia';
import i18n from './langs/i18n';

import App from './App.vue';
import router from './router';

const app = createApp(App);

app.use(createPinia());
app.use(router);
app.use(ElementPlus);
app.use(VueAxios, axios);

app.use(i18n);
app.mount('#app');
