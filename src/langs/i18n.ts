import { createI18n } from 'vue-i18n';
import en from './en.json';
import ja from './ja.json';

const i18n = createI18n({
  legacy: false,
  locale: 'en',
  fallbackLocale: 'en',
  allowComposition: true,
  messages: {
    ja: ja,
    en: en
  },
});
export default i18n;
